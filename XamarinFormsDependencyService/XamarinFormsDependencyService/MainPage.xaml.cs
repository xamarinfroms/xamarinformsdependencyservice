﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace XamarinFormsDependencyService
{
    public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
		}

        public async void OnClick(object sender,EventArgs e)
        {

            var loading=DependencyService.Get<ILoadingView>();


            loading.Show();
            await Task.Delay(3000);
            loading.Dismiss();

        }

    }
}
