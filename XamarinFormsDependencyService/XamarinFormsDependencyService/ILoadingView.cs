﻿

namespace XamarinFormsDependencyService
{

    public  interface ILoadingView
    {
        void Show();
        void Dismiss();
    }

}
