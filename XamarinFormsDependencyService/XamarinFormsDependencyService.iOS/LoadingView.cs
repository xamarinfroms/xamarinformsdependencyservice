﻿using BigTed;
using Xamarin.Forms;
using XamarinFormsDependencyService.iOS;

[assembly: Dependency(typeof(LoadingView))]
namespace XamarinFormsDependencyService.iOS
{

    public class LoadingView : ILoadingView
    {
        public void Dismiss()
        {
            if (BTProgressHUD.IsVisible)
                BTProgressHUD.Dismiss();
        }

        public void Show()
        {
            if (!BTProgressHUD.IsVisible)
                BTProgressHUD.Show(maskType: ProgressHUD.MaskType.Black);
        }
    }
}
