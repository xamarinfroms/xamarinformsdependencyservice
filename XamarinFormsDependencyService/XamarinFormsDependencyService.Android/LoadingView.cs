﻿using Android.App;
using Xamarin.Forms;
using XamarinFormsDependencyService.Droid;


[assembly: Dependency(typeof(LoadingView))]
namespace XamarinFormsDependencyService.Droid
{

    public class LoadingView : ILoadingView
    {
        private static ProgressDialog ProgressDialog;
        public void Dismiss()
        {
            if (ProgressDialog != null && ProgressDialog.IsShowing)
            {
                ProgressDialog.Dismiss();
            }
        }
        public void Show()
        {
            if (ProgressDialog == null)
            {
                ProgressDialog = new ProgressDialog(Forms.Context);
                ProgressDialog.SetCancelable(false);
                ProgressDialog.SetCanceledOnTouchOutside(false);
                ProgressDialog.SetMessage("Loading...");
            }
            if (!ProgressDialog.IsShowing)
            {
                ProgressDialog.Show();
            }
        }
    }

}